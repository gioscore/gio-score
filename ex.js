const doc = {
  swagger: "2.0",
  info: {
    description: "This is a API doc for Gioscore.",
    version: "1",
    title: "DOC API GIOSCORE",
    termsOfService: "http://gioscore.com/",
    contact: {
      email: "gioscore@gmail.com",
    },
  },
  host: "https://api.gioscore.com",
  basePath: "/v2",
  tags: [
    {
      name: "country",
      description: "Everything about your countries",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "league",
      description: "Everything about your leagues",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "season",
      description: "Everything about your seasons",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },

    {
      name: "standing",
      description: "Everything about your standings",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "topscorer",
      description: "Everything about your topscorers",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "fixtures",
      description: "Everything about your fixtures",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "team",
      description: "Everything about your teams",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "player",
      description: "Everything about your players",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "coaches",
      description: "Everything about your coachess",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "referee",
      description: "Everything about your referees",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "search",
      description: "Everything about your search",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
    {
      name: "sitemap",
      description: "Everything about your sitemap",
      externalDocs: {
        description: "Find out more",
        url: "https://api.gioscore.com",
      },
    },
  ],
  schemes: ["https", "http"],
  paths: {
    "/countries": {
      get: {
        tags: ["country"],
        summary: "Get all countries",
        description: "Get all countries",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:countries", "read:countries"],
          },
        ],
      },
    },
    "/countries/{Id}": {
      get: {
        tags: ["country"],
        summary: "Get country detail",
        description: "Get country detail",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of country that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:countries", "read:countries"],
          },
        ],
      },
    },

    "/leagues": {
      get: {
        tags: ["league"],
        summary: "Get all leagues",
        description: "Get total and all basic information for leagues",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:leagues", "read:leagues"],
          },
        ],
      },
    },
    "/leagues/popular": {
      get: {
        tags: ["league"],
        summary: "Get popular leagues",
        description: "Used it to show left-bar menu",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:leagues", "read:leagues"],
          },
        ],
      },
    },
    "/leagues/top": {
      get: {
        tags: ["league"],
        summary: "Get 100 popular leagues",
        description: "Used it to show on tournament-page",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:leagues", "read:leagues"],
          },
        ],
      },
    },
    "/leagues/{Id}": {
      get: {
        tags: ["league"],
        summary: "Get information for league",
        description: "Get information for league",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of league that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:leagues", "read:leagues"],
          },
        ],
      },
    },
    "/leagues/about/{Id}": {
      get: {
        tags: ["league"],
        summary: "Get about information for league",
        description: "Used to load on SSR",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of league that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:leagues", "read:leagues"],
          },
        ],
      },
    },
    "/leagues/country/{countryId}": {
      get: {
        tags: ["league"],
        summary: "Get all leagues on a country",
        description: "Get all leagues on a country",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "countryId",
            in: "path",
            description: "Id of country that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:leagues", "read:leagues"],
          },
        ],
      },
    },
    "/leagues/salary/{Id}": {
      get: {
        tags: ["league"],
        summary: "Get list teams salary",
        description: "Get list teams salary on a leagues",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "countryId",
            in: "path",
            description: "Id of leagues that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:leagues", "read:leagues"],
          },
        ],
      },
    },

    "/seasons/{Id}": {
      get: {
        tags: ["season"],
        summary: "Get data on a season",
        description: "get information season and leagues on current",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:seasons", "read:seasons"],
          },
        ],
      },
    },
    "/seasons/teams/{Id}": {
      get: {
        tags: ["season"],
        summary: "Get teams on a season",
        description: "Get teams on a season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:seasons", "read:seasons"],
          },
        ],
      },
    },
    "/seasons/rankings/{Id}": {
      get: {
        tags: ["season"],
        summary: "Get ranking players on a season",
        description: "Get ranking players on a season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:seasons", "read:seasons"],
          },
        ],
      },
    },
    "/seasons/top-rating/{Id}": {
      get: {
        tags: ["season"],
        summary: "Get top-rating players on a season",
        description: "Get top-rating players on a season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:seasons", "read:seasons"],
          },
        ],
      },
    },
    "/seasons/winner/{Id}": {
      get: {
        tags: ["season"],
        summary: "Get team win a season",
        description: "Get team win a season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:seasons", "read:seasons"],
          },
        ],
      },
    },
    "/seasons/players-per-season/{Id}": {
      get: {
        tags: ["season"],
        summary: "Get all players per season",
        description: "Get all players per season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:seasons", "read:seasons"],
          },
        ],
      },
    },

    "/standings/top": {
      get: {
        tags: ["standing"],
        summary: "Get popular standings leagues",
        description: "get popular and visited standings leagues",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:standings", "read:standings"],
          },
        ],
      },
    },
    "/standings/{Id}": {
      get: {
        tags: ["standing"],
        summary: "Get standings on a season",
        description: "get standings a season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season standings that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:standings", "read:standings"],
          },
        ],
      },
    },
    "/standings/by-match/{Id}": {
      get: {
        tags: ["standing"],
        summary: "Get standings on match's season",
        description: "get standings a match's season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description:
              "ID of match to get standings  that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:standings", "read:standings"],
          },
        ],
      },
    },
    "/top-scorers/{Id}": {
      get: {
        tags: ["topscorer"],
        summary: "Get top-scorer on a season",
        description: "get top-scorer a season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of season top-scorer that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:topscorers", "read:topscorers"],
          },
        ],
      },
    },

    "/fixtures/featured-match": {
      get: {
        tags: ["fixtures"],
        summary: "Get random a match on season",
        description: "Get random a match on season to show on league-page",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/live-score": {
      get: {
        tags: ["fixtures"],
        summary: "Refresh live-score",
        description: "we have cron-tab to run this end-point auto per 2 min",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "Get data a match",
        description: "Get data a match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "ID of match that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/current/{code}": {
      get: {
        tags: ["fixtures"],
        summary: "Get id match from code",
        description:
          "get nearest head-to-head of 2 teams (2 id teams were uncode)",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "code",
            in: "path",
            description: "uncode of 2 id teams that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/basic/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get basic information fixtures",
        description: "get basic information fixtures to load on SSR",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "code",
            in: "path",
            description: "Id fixtures that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/dynamic/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get dynamic information fixtures",
        description:
          "get dynamic information fixtures to refresh data for matches playing",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "code",
            in: "path",
            description: "Id fixtures that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/head2head/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get head-to-head 2 teams in the match",
        description: "get head-to-head 2 teams in the match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "code",
            in: "path",
            description: "Id fixtures that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/head2head-custom/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get head-to-head 2 teams in the match",
        description: "get head-to-head 2 teams in the match, we have some ",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "code",
            in: "path",
            description: "Id fixtures that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/multi/{Ids}": {
      get: {
        tags: ["fixtures"],
        summary: "get list fixtures",
        description: "get list fixtures",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Ids",
            in: "path",
            description: "list Id fixtures that needs to be fetched",
            required: true,
            type: "string",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/season/{SeasonId}": {
      get: {
        tags: ["fixtures"],
        summary: "get list fixtures",
        description: "get list fixtures",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "SeasonId",
            in: "path",
            description: "SeasonId on fixtures that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "id_round",
            in: "query",
            description: "id_round on fixtures that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "id_stage",
            in: "query",
            description: "id_stage on fixtures that needs to be fetched",
            required: true,
            type: "string",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/basic/{startTime}/{endTime}/{teamId}": {
      get: {
        tags: ["fixtures"],
        summary: "get fixtures of team between range time",
        description: "get fixtures of team between range time",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "startTime",
            in: "path",
            description: "start time that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "endTime",
            in: "path",
            description: "end time that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "teamId",
            in: "path",
            description: "id team that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/date/live-score/{timestamp}": {
      get: {
        tags: ["fixtures"],
        summary: "get list matches live-score",
        description:
          "get list matches live-score, this end-point support for home-page, to load matches on current day",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "timestamp",
            in: "path",
            description: "current time to get list fixtures",
            required: true,
            type: "string",
          },
          {
            name: "lang",
            in: "query",
            description: "flag used to set priority for fixtures on country",
            required: true,
            type: "string",
          },
          {
            name: "priorities",
            in: "query",
            description:
              "flag used to set priority for list latest leagues visited ",
            required: true,
            type: "string",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/date/{timeStart}/{timeEnd}": {
      get: {
        tags: ["fixtures"],
        summary: "get list matches live-score",
        description:
          "get list matches live-score, this end-point support for home-page, to load matches not current day (next time or prev time)",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "timeStart",
            in: "path",
            description: "start time to get fixtures",
            required: true,
            type: "string",
          },
          {
            name: "timeEnd",
            in: "path",
            description: "end time to get fixtures",
            required: true,
            type: "string",
          },
          {
            name: "lang",
            in: "query",
            description: "flag used to set priority for fixtures on country",
            required: true,
            type: "string",
          },
          {
            name: "priorities",
            in: "query",
            description:
              "flag used to set priority for list latest leagues visited ",
            required: true,
            type: "string",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/latest/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get the latest matches 2 team on current match",
        description: "get the latest matches 2 team on current match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description:
              "id match that latest matches 2 teams needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/manager-h2h/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get stats head-to-head team's mangers on current match",
        description: "get stats head-to-head team's mangers on current match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description:
              "id match that head-to-head team's mangers needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/ranking/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get stats ranking players on 2 team on current match",
        description: "get stats ranking players on 2 team on current match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description:
              "id match that list players ranking needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/amp/{Id}": {
      get: {
        tags: ["fixtures"],
        summary: "get amp information on current match",
        description: "get amp information on current match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "id match that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },
    "/fixtures/friendly-league/{seasonId}": {
      get: {
        tags: ["fixtures"],
        summary: "get list matches for friendly-leagues on season",
        description: "get amp information on current match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "seasonId",
            in: "path",
            description: "id match that matches needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "page",
            in: "query",
            description: "index pagination that matches needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:fixtures", "read:fixtures"],
          },
        ],
      },
    },

    "/teams/top": {
      get: {
        tags: ["team"],
        summary: "Get top 100 popular teams",
        description: "Get top 100 popular teams",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/teams/{Id}": {
      get: {
        tags: ["team"],
        summary: "Get information a team",
        description: "Get information a team",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "id team that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/teams/info/{Id}": {
      get: {
        tags: ["team"],
        summary: "Get basic information a team",
        description: "Get basic information a team to load SSR",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "id team that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/teams/stats/{teamId}/{seasonId}": {
      get: {
        tags: ["team"],
        summary: "Get list stats players team on season",
        description: "Get list stats players team on season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "seasonId",
            in: "path",
            description: "id team that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "teamId",
            in: "path",
            description: "id season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "include",
            in: "query",
            description:
              "list fields need get, ex: goals,rating,appearences,...",
            required: true,
            type: "string",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/teams/best-team-round/{seasonId}/{idRound}": {
      get: {
        tags: ["team"],
        summary: "Get best team on round",
        description: "Get best team on round",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "seasonId",
            in: "path",
            description: "id season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "idRound",
            in: "path",
            description: "id round that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/teams/best-team-stage/{seasonId}/{idStage}": {
      get: {
        tags: ["team"],
        summary: "Get best team on stage",
        description: "Get best team on stage",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "seasonId",
            in: "path",
            description: "id season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "idStage",
            in: "path",
            description: "id stage that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/teams/{startTime}{endTime}/{id}": {
      get: {
        tags: ["team"],
        summary: "Get team's fixtures on range time",
        description: "Get team's fixtures on range time",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "id",
            in: "path",
            description: "id team that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "startTime",
            in: "path",
            description: "start time that needs to be fetched, ex: 2020-10-10",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "endTime",
            in: "path",
            description: "start time that needs to be fetched, ex: 2020-10-20",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/teams/multi/{ids}": {
      get: {
        tags: ["team"],
        summary: "Get multi information team",
        description: "Get multi information team, used on compare-page",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "ids",
            in: "path",
            description: "list id teams that needs to be fetched",
            required: true,
            type: "string",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/team-stats/{Id}": {
      get: {
        tags: ["team"],
        summary: "Get stats on team",
        description: "Get stats on team",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "id",
            in: "path",
            description: "list id teams that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "include",
            in: "query",
            description:
              "list fields stat that needs to be fetched, include: stats, latest, upcoming, sidelined",
            required: true,
            type: "string",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },
    "/team-stats/about/{Id}": {
      get: {
        tags: ["team"],
        summary: "Get information about-team",
        description: "Get information about-team to load SSR",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id team that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:teams", "read:teams"],
          },
        ],
      },
    },

    "/players/top": {
      get: {
        tags: ["player"],
        summary: "Get top 100 popular players",
        description: "Get top 100 popular players",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },

    "/players/filter": {
      get: {
        tags: ["player"],
        summary: "filter stats player by team and season",
        description: "filter stats player by team and season",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/players/{Id}": {
      get: {
        tags: ["player"],
        summary: "get information a player",
        description: "get information a player",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id player that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/players/basic/{Id}": {
      get: {
        tags: ["player"],
        summary: "get basic information a player",
        description: "get basic information a player to support load SSR",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id player that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/lineups/{Id}": {
      get: {
        tags: ["player"],
        summary: "get lineups a player",
        description: "get lineups a player to support load SSR",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id player that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/player-rival/{Id}": {
      get: {
        tags: ["player"],
        summary: "get history player's player-rival",
        description: "get history player's player-rival",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id player that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/stats-players/{Id}": {
      get: {
        tags: ["player"],
        summary: "get lineups detail player",
        description: "get lineups detail player",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id player that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/stats-players/stats/{Id}": {
      get: {
        tags: ["player"],
        summary: "get all stat seasons player",
        description: "get all stat seasons player",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id player that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/stats-players/stats/{Id}/{idSeason}": {
      get: {
        tags: ["player"],
        summary: "get stat a season player",
        description: "get stat a season player",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id player that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
          {
            name: "idSeason",
            in: "path",
            description: "Id season that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:players", "read:players"],
          },
        ],
      },
    },
    "/coaches/top": {
      get: {
        tags: ["coach"],
        summary: "get information top coach",
        description: "get information top coach",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:coaches", "read:coaches"],
          },
        ],
      },
    },
    "/coaches/{Id}": {
      get: {
        tags: ["coach"],
        summary: "get information a coach",
        description: "get information a coach",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id coach that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:coaches", "read:coaches"],
          },
        ],
      },
    },
    "/coaches/info/{Id}": {
      get: {
        tags: ["coach"],
        summary: "get basic information a coach",
        description: "get basic information a coach",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id coach that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:coaches", "read:coaches"],
          },
        ],
      },
    },
    "/coaches/fixtures/{Id}": {
      get: {
        tags: ["coach"],
        summary: "get coach's history fixtures",
        description: "get coach's history fixtures",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id coach that needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:coaches", "read:coaches"],
          },
        ],
      },
    },
    "/referees": {
      get: {
        tags: ["referee"],
        summary: "get top referees",
        description: "get top referees",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:referee", "read:referee"],
          },
        ],
      },
      "/referees/{Id}": {
        get: {
          tags: ["referee"],
          summary: "get data referees",
          description: "get data referees",
          operationId: "",
          consumes: ["multipart/form-data"],
          produces: ["application/json"],
          parameters: [
            {
              name: "Id",
              in: "path",
              description: "Id referee that needs to be fetched",
              required: true,
              type: "integer",
              maximum: 10,
              minimum: 1,
              format: "int64",
            },
          ],
          responses: {
            200: {
              description: "success",
              schema: {},
            },
          },
          security: [
            {
              petstore_auth: ["write:referee", "read:referee"],
            },
          ],
        },
      },
    },
    "/referees/fixture/{Id}": {
      get: {
        tags: ["referee"],
        summary: "get data referees in the match",
        description: "get data referees in the match",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "Id",
            in: "path",
            description: "Id match that data referee needs to be fetched",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:referee", "read:referee"],
          },
        ],
      },
    },
    "/search/{params}": {
      get: {
        tags: ["search"],
        summary: "search for all object",
        description:
          "search leagues,teams,players and coaches, apply on header autocomplete",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total object needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },

    "/search/compare/{params}": {
      get: {
        tags: ["search"],
        summary: "search for teams and players",
        description:
          "search for teams and players, used to get data on compare page",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total object needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },
    "/search/players/{params}": {
      get: {
        tags: ["search"],
        summary: "search for players",
        description: "search for players",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total players needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },

    "/search/coaches/{params}": {
      get: {
        tags: ["search"],
        summary: "search for coaches",
        description: "search for coaches",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total coaches needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },

    "/search/teams/{params}": {
      get: {
        tags: ["search"],
        summary: "search for teams",
        description: "search for teams",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total teams needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },

    "/search/leagues/{params}": {
      get: {
        tags: ["search"],
        summary: "search for leagues",
        description: "search for leagues",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total leagues needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },

    "/search/sponsors/{params}": {
      get: {
        tags: ["search"],
        summary: "search for sponsors",
        description: "search for sponsors",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total sponsors needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },
    "/search/referees/{params}": {
      get: {
        tags: ["search"],
        summary: "search for referees",
        description: "search for referees",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "params",
            in: "path",
            description:
              "the text to search the information that needs to be fetched",
            required: true,
            type: "string",
          },
          {
            name: "per_obj",
            in: "query",
            description: "total referees needs to be fetched once",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:search", "read:search"],
          },
        ],
      },
    },
    "/sitemap/global": {
      get: {
        tags: ["sitemap"],
        summary: "get information to for sitemap global",
        description: "get information to for sitemap global",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:sitemap", "read:sitemap"],
          },
        ],
      },
    },
    "/sitemap/leagues": {
      get: {
        tags: ["sitemap"],
        summary: "get information to for sitemap leagues",
        description: "get information to for sitemap leagues",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "page",
            in: "query",
            description: "index page to get data",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:sitemap", "read:sitemap"],
          },
        ],
      },
    },
    "/sitemap/teams": {
      get: {
        tags: ["sitemap"],
        summary: "get information to for sitemap teams",
        description: "get information to for sitemap teams",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "page",
            in: "query",
            description: "index page to get data",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:sitemap", "read:sitemap"],
          },
        ],
      },
    },
    "/sitemap/players": {
      get: {
        tags: ["sitemap"],
        summary: "get information to for sitemap players",
        description: "get information to for sitemap players",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "page",
            in: "query",
            description: "index page to get data",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:sitemap", "read:sitemap"],
          },
        ],
      },
    },
    "/sitemap/coaches": {
      get: {
        tags: ["sitemap"],
        summary: "get information to for sitemap coaches",
        description: "get information to for sitemap coaches",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "page",
            in: "query",
            description: "index page to get data",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:sitemap", "read:sitemap"],
          },
        ],
      },
    },
    "/sitemap/fixtures": {
      get: {
        tags: ["sitemap"],
        summary: "get information to for sitemap fixtures",
        description: "get information to for sitemap global",
        operationId: "",
        consumes: ["multipart/form-data"],
        produces: ["application/json"],
        parameters: [
          {
            name: "page",
            in: "query",
            description: "index page to get data",
            required: true,
            type: "integer",
            maximum: 10,
            minimum: 1,
            format: "int64",
          },
        ],
        responses: {
          200: {
            description: "success",
            schema: {},
          },
        },
        security: [
          {
            petstore_auth: ["write:sitemap", "read:sitemap"],
          },
        ],
      },
    },
  },
  securityDefinitions: {
    api_key: {
      type: "apiKey",
      name: "api_key",
      in: "header",
    },
    petstore_auth: {
      type: "oauth2",
      authorizationUrl: "https://petstore.swagger.io/oauth/authorize",
      flow: "implicit",
      scopes: {
        "read:countries": "",
        "write:countries": "",
        "read:leagues": "",
        "write:leagues": "",
        "read:seasons": "",
        "write:seasons": "",
        "read:teams": "",
        "write:teams": "",
        "read:standing": "",
        "write:standing": "",
        "read:topscorer": "",
        "write:topscorer": "",
        "read:fixtures": "",
        "write:fixtures": "",
        "read:player": "",
        "write:player": "",
        "read:coaches": "",
        "write:coaches": "",
        "read:referee": "",
        "write:referee": "",
        "read:search": "",
        "write:search": "",
        "read:sitemap": "",
        "write:sitemap": "",
      },
    },
  },
  definitions: {},
  externalDocs: {
    description: "Find out more about Swagger",
    url: "http://swagger.io",
  },
};

export default doc;
